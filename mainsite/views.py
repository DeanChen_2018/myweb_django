from django.shortcuts import render
#from django.http import HttpResponse
from .models import Post
from datetime import datetime
from django.shortcuts import redirect
# Create your views here.

def homepage (request):
		
	"""
	#change use template

	posts = Post.objects.all()
	posts_list =list ()
	for count, post in enumerate(posts):
		posts_list.append("No.{}:".format(str(count)) + str(post) + "<br>")
	return HttpResponse(posts_list)
	"""
	posts = Post.objects.all()
	now = datetime.now()
	return	render(request, 'index.html', locals())

def showpost(request, slug):
	try:
		post = Post.objects.get (slug = slug)
		if post is not None:
			return render (request, 'post.html', locals())
	except:
		return redirect ('/')
